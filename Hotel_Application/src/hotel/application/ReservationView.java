/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import java.time.LocalDate;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class ReservationView extends Application {

    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("Reservation");
        stage.setScene(scene);
        stage.show();

        DatePicker dp = new DatePicker();
        dp.setOnAction(e -> {

            LocalDate date = dp.getValue();
            System.out.println("Selected date: " + date);

        });
    }
    

    public static void main(String[] args) {
        launch(args);
    }

}
